#include "hex_can.h"
#include "freertos/FreeRTOS.h"
#include "driver/gpio.h"
#include "driver/can.h"
#include "esp_log.h"

#define CAN_RX (4)
#define CAN_TX (5)
#define CAN_KLINE_ENABLE   (27)
#define HEX_CAN_TAG "HEX_CAN"


hex_return_type_e HEX_CAN_Transmit(void)
{
   //can_message_t sMsg = {.data_length_code = 1, .identifier = 0x555, .flags = CAN_MSG_FLAG_SELF};  //Self test
#if 1
   can_message_t sMsg;

   sMsg.data_length_code = 8;
   sMsg.identifier = 0x7DF;
   sMsg.flags = CAN_MSG_FLAG_NONE;
   sMsg.data[0] = 0x02;
   sMsg.data[1] = 0x01;
   sMsg.data[2] = 0x01;
   sMsg.data[3] = 0x00;
   sMsg.data[4] = 0x00;
   sMsg.data[5] = 0x00;
   sMsg.data[6] = 0x00;
   sMsg.data[7] = 0x00;
#endif

   if (can_transmit(&sMsg, portMAX_DELAY) != ESP_OK) {
      ESP_LOGE(HEX_CAN_TAG, "Can not transmit frame");
      return HEX_FAILURE;
   }
 
   ESP_LOGI(HEX_CAN_TAG, "Message transmitted");
   return HEX_SUCCESS;

}


hex_return_type_e HEX_CAN_Receive(void)
{
   can_message_t message;

   ESP_LOGI(HEX_CAN_TAG, "Trying to receive message");

   //Wait for message to be received
   if (can_receive(&message, pdMS_TO_TICKS(1000)) == ESP_OK) {
       ESP_LOGI(HEX_CAN_TAG, "Message received");
   } else {
       ESP_LOGE(HEX_CAN_TAG, "Failure to receive message");
       return HEX_FAILURE;
   }

   //Process received message
   if (message.flags & CAN_MSG_FLAG_EXTD) {
       ESP_LOGI(HEX_CAN_TAG, "Message is in Extended Format");
   } else {
       ESP_LOGI(HEX_CAN_TAG, "Message is in Standard Format");
   }

   ESP_LOGI(HEX_CAN_TAG, "ID is %d\n", message.identifier);
   if (!(message.flags & CAN_MSG_FLAG_RTR)) {
       for (int i = 0; i < message.data_length_code; i++) {
           ESP_LOGI(HEX_CAN_TAG, "Data byte %d = %d\n", i, message.data[i]);
       }
   }

   return HEX_SUCCESS;
}


hex_return_type_e HEX_CAN_Init(void)
{
   gpio_config_t sConfig;
   esp_err_t eReturn;

   //Initialise GPIO connected to CAN drivers
   sConfig.intr_type = GPIO_PIN_INTR_DISABLE;
   sConfig.mode = GPIO_MODE_OUTPUT;
   sConfig.pin_bit_mask = (1<<CAN_KLINE_ENABLE);
   sConfig.pull_down_en = 0;
   sConfig.pull_up_en = 0;
   eReturn = gpio_config(&sConfig);
   if (eReturn != ESP_OK) {
      ESP_LOGE(HEX_CAN_TAG, "Error initialising GPIO %d", eReturn);
      return HEX_FAILURE;
   }

   //Enable driver
   eReturn = gpio_set_level(CAN_KLINE_ENABLE, 0);   
   if (eReturn != ESP_OK) {
      ESP_LOGE(HEX_CAN_TAG, "Error enabling driver %d", eReturn);
      return HEX_FAILURE;
   }

   //Initialize configuration structures using macro initializers
    can_general_config_t g_config = CAN_GENERAL_CONFIG_DEFAULT(CAN_TX, CAN_RX, CAN_MODE_NORMAL);
    can_timing_config_t t_config = CAN_TIMING_CONFIG_500KBITS();
    can_filter_config_t f_config = CAN_FILTER_CONFIG_ACCEPT_ALL();

    //Install CAN driver
    if (can_driver_install(&g_config, &t_config, &f_config) == ESP_OK) {
         ESP_LOGI(HEX_CAN_TAG, "Driver successfully installed");
    } else {
         ESP_LOGE(HEX_CAN_TAG, "Failed to install driver");
        return HEX_FAILURE;
    }

    //Start CAN driver
    if (can_start() == ESP_OK) {
      ESP_LOGI(HEX_CAN_TAG, "Driver started");
    } else {
      ESP_LOGI(HEX_CAN_TAG, "Failed to start driver");
      return HEX_FAILURE;
    }

   return HEX_SUCCESS;
}

