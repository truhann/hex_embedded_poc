#include "hex_types.h"

hex_return_type_e HEX_CAN_Init(void);
hex_return_type_e HEX_CAN_Transmit(void);
hex_return_type_e HEX_CAN_Receive(void);

