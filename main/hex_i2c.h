#include "hex_types.h"

hex_return_type_e HEX_I2C_Init(bool bSlow);

hex_return_type_e HEX_I2C_ReadRTC(uint8_t* data_rd, size_t size);

hex_return_type_e HEX_I2C_ReadACC(uint8_t* data_rd, size_t size);

hex_return_type_e HEX_I2C_ReadSHA(uint8_t* data_rd, size_t size);

