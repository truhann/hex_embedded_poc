/**
  ******************************************************************************
  * @file    vs_types.h
  * @brief   Hex custom types.             
  ******************************************************************************
  * @attention
  *
  * <h3> &copy; COPYRIGHT(c) 2018 Hex PTY Ltd </h3>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are NOT permitted.
  *
  *
  ******************************************************************************
  */ 
	
#ifndef __HEX_TYPES_H__
#define __HEX_TYPES_H__

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

typedef char * hex_uri_t; /**<Debug URI */

/** Hex return type enum. 
 */
typedef enum {
	HEX_SUCCESS = 0x00, 		/**< success */
	HEX_FAILURE = 0x01,		/**< failure */
	HEX_IN_PROGRESS = 0x02	/**< in progress */
} hex_return_type_e;

#endif

