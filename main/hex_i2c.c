#include "hex_i2c.h"
#include "driver/i2c.h"
#include "esp_log.h"

#define HEX_I2C_TAG "HEX_I2C"
#define ESP_SLAVE_ADDR_RTC (0x51)
#define ESP_SLAVE_ADDR_ACC (0x18)
#define ESP_SLAVE_ADDR_SHA (0xC8>>1)

hex_return_type_e HEX_I2C_Init(bool bSlow)
{
   esp_err_t eResult;


   //Make sure no driver is currently installed
    (void)i2c_driver_delete(I2C_NUM_0);

    int i2c_master_port = I2C_NUM_0;
    i2c_config_t conf;
    conf.mode = I2C_MODE_MASTER;
    conf.sda_io_num = 22;
    conf.sda_pullup_en = GPIO_PULLUP_ENABLE;
    conf.scl_io_num = 23;
    conf.scl_pullup_en = GPIO_PULLUP_ENABLE;
    if (bSlow == true) conf.master.clk_speed = 100000;
    else conf.master.clk_speed = 400000;
    eResult = i2c_param_config(i2c_master_port, &conf);
   if (eResult != ESP_OK) {
      ESP_LOGE(HEX_I2C_TAG, "Can not set i2c parameter %d", eResult);
      return HEX_FAILURE;
   }
    eResult = i2c_driver_install(i2c_master_port, conf.mode,
                       0,
                       0, 0);
   if (eResult != ESP_OK) {
      ESP_LOGE(HEX_I2C_TAG, "Can not install i2c driver $%d", eResult);
      return HEX_FAILURE;
   }

   return HEX_SUCCESS;
}

hex_return_type_e HEX_I2C_ReadRTC(uint8_t* data_rd, size_t size)
{
   esp_err_t eResult;

    if (size == 0) {
         ESP_LOGW(HEX_I2C_TAG, "Trying to read 0 bytes");
        return HEX_SUCCESS;
    }
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
   
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, ( ESP_SLAVE_ADDR_RTC << 1 ) | I2C_MASTER_WRITE, 1);   //RTC I2C address
    i2c_master_write_byte(cmd, 0x07, 1);                                         //RTC register address
    i2c_master_stop(cmd);
    eResult = i2c_master_cmd_begin(I2C_NUM_0, cmd, 1000 / portTICK_RATE_MS);
    if (eResult != ESP_OK) ESP_LOGE(HEX_I2C_TAG, "Error starting cmd: %d", eResult);

    i2c_cmd_link_delete(cmd);
    cmd = i2c_cmd_link_create();

    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, ( ESP_SLAVE_ADDR_RTC << 1 ) | I2C_MASTER_READ, 1);   //RTC I2C address
    i2c_master_read_byte(cmd, data_rd, 1);
    i2c_master_stop(cmd);
    eResult = i2c_master_cmd_begin(I2C_NUM_0, cmd, 1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);
    if (eResult != ESP_OK) ESP_LOGE(HEX_I2C_TAG, "Error starting cmd: %d", eResult);

    return (eResult == ESP_OK) ? HEX_SUCCESS : HEX_FAILURE;
}



hex_return_type_e HEX_I2C_ReadACC(uint8_t* data_rd, size_t size)
{
   esp_err_t eResult;

    if (size == 0) {
         ESP_LOGW(HEX_I2C_TAG, "Trying to read 0 bytes");
        return HEX_SUCCESS;
    }
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
   
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, ( ESP_SLAVE_ADDR_ACC << 1 ) | I2C_MASTER_WRITE, 1);   //RTC I2C address
    i2c_master_write_byte(cmd, 0x0F, 1);                                         //RTC register address
    i2c_master_stop(cmd);
    eResult = i2c_master_cmd_begin(I2C_NUM_0, cmd, 1000 / portTICK_RATE_MS);
    if (eResult != ESP_OK) ESP_LOGE(HEX_I2C_TAG, "Error starting cmd: %d", eResult);

    i2c_cmd_link_delete(cmd);
    cmd = i2c_cmd_link_create();

    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, ( ESP_SLAVE_ADDR_ACC << 1 ) | I2C_MASTER_READ, 1);   //RTC I2C address
    i2c_master_read_byte(cmd, data_rd, 1);
    i2c_master_stop(cmd);
    eResult = i2c_master_cmd_begin(I2C_NUM_0, cmd, 1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);
    if (eResult != ESP_OK) ESP_LOGE(HEX_I2C_TAG, "Error starting cmd: %d", eResult);

    return (eResult == ESP_OK) ? HEX_SUCCESS : HEX_FAILURE;
}

hex_return_type_e HEX_I2C_ReadSHA(uint8_t* data_rd, size_t size)
{
   esp_err_t eResult;

    if (size == 0) {
         ESP_LOGW(HEX_I2C_TAG, "Trying to read 0 bytes");
        return HEX_SUCCESS;
    }

    //Wake ATSHA device (Pull SDA low for min 60us)
#if 0
    gpio_set_level(22, 0);
    vTaskDelay(1/portTICK_PERIOD_MS); 
    gpio_set_level(22, 1);
#endif
    if (HEX_I2C_Init(true) != HEX_SUCCESS) ESP_LOGE(HEX_I2C_TAG, "Can not init I2C interface %d", 1);

    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
   
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, ( 0 << 1 ) | I2C_MASTER_WRITE, 1);   //RTC I2C address
    i2c_master_stop(cmd);
    (void)i2c_master_cmd_begin(I2C_NUM_0, cmd, 1000 / portTICK_RATE_MS);
    //eResult = i2c_master_cmd_begin(I2C_NUM_0, cmd, 1000 / portTICK_RATE_MS);
    //eResult != ESP_OK) ESP_LOGE(HEX_I2C_TAG, "Error starting cmd: %d", eResult);

    i2c_cmd_link_delete(cmd);

    vTaskDelay(10/portTICK_PERIOD_MS);  //Wait tWHI (min 2.5ms)

    if (HEX_I2C_Init(false) != HEX_SUCCESS) ESP_LOGE(HEX_I2C_TAG, "Can not init I2C interface %d", 1);

    cmd = i2c_cmd_link_create();
   
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, ( ESP_SLAVE_ADDR_SHA << 1 ) | I2C_MASTER_WRITE, 1);   //RTC I2C address
    i2c_master_write_byte(cmd, 0x0F, 1);                                         //RTC register address
    i2c_master_stop(cmd);
    eResult = i2c_master_cmd_begin(I2C_NUM_0, cmd, 1000 / portTICK_RATE_MS);
    if (eResult != ESP_OK) ESP_LOGE(HEX_I2C_TAG, "Error starting cmd: %d", eResult);

    i2c_cmd_link_delete(cmd);
    cmd = i2c_cmd_link_create();

    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, ( ESP_SLAVE_ADDR_SHA << 1 ) | I2C_MASTER_READ, 1);   //RTC I2C address
    i2c_master_read_byte(cmd, data_rd, 1);
    i2c_master_stop(cmd);
    eResult = i2c_master_cmd_begin(I2C_NUM_0, cmd, 1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);
    if (eResult != ESP_OK) ESP_LOGE(HEX_I2C_TAG, "Error starting cmd: %d", eResult);

    return (eResult == ESP_OK) ? HEX_SUCCESS : HEX_FAILURE;
}


